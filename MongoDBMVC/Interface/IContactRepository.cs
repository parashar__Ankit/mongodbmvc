﻿using MongoDBMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MongoDBMVC.Interface
{
    public interface IContactRepository
    {
        Task CreateContact(Contact contact);
        Task<IEnumerable<Contact>> GetContact();
    }
}