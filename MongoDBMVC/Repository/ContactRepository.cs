﻿using MongoDB.Driver;
using MongoDBMVC.Interface;
using MongoDBMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MongoDBMVC.Repository
{
    public class ContactRepository : IContactRepository
    {
        MongoDbContext db = new MongoDbContext();
        public async Task CreateContact(Contact contact)
        {
            try
            {
                db.Contact.InsertOneAsync(contact);
            }
            catch
            {
                throw;
            }


        }
        public async Task<IEnumerable<Contact>> GetContact()
        {
            try
            {
                return await db.Contact.Find(_ => true).ToListAsync();
            }
            catch
            {
                throw;
            }
        }
    }
}