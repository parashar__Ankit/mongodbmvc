﻿using MongoDB.Driver;

namespace MongoDBMVC.Models
{
    public class MongoDbContext
    {
        private readonly IMongoDatabase _mongoDb;
        public MongoDbContext()
        {
            var client = new MongoClient("mongodb+srv://ankit-parashar:AnkitParashar700@projectdata-h59gp.mongodb.net/test?retryWrites=true&w=majority");

            _mongoDb = client.GetDatabase("AnkitDB");
        }
        public IMongoCollection<Employee> Employee
        {
            get
            {
                return _mongoDb.GetCollection<Employee>("Employee");
            }
        }

        public IMongoCollection<Contact> Contact
        {
            get
            {

                return _mongoDb.GetCollection<Contact>("Contact");
            }
        }
    }
}